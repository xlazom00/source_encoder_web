#-*- coding: utf-8 -*-
import pprint
import urlparse
import urllib
import time
import json
import sqlobject
from sqlobject import *

import MySQLdb.converters

defaultStreamDuration = 10
# add 5min to every event
defaultEventSuffixTime = 5 * 60
liveStreamParts = 8

from json import JSONEncoder

def _default(self, obj):
    return getattr(obj.__class__, "to_json", _default.default)(obj)

_default.default = JSONEncoder().default # save unmodified default
JSONEncoder.default = _default # replacement


def _mysql_timestamp_converter(raw):
         """Convert a MySQL TIMESTAMP to a floating point number representing
         the seconds since the Un*x Epoch. It uses custom code the input seems
         to be the new (MySQL 4.1+) timestamp format, otherwise code from the
         MySQLdb module is used."""
         if raw[4] == '-':
             return time.mktime(time.strptime(raw, '%Y-%m-%d %H:%M:%S'))
         else:
             return MySQLdb.converters.mysql_timestamp_converter(raw)

#print(str(globals()))

if "MySQLConnection" not in globals():
#     print("new registration for MySQLConnection")
    conversions = MySQLdb.converters.conversions.copy()
    conversions[MySQLdb.constants.FIELD_TYPE.TIMESTAMP] = _mysql_timestamp_converter

    MySQLConnection = sqlobject.mysql.builder()
    connection = MySQLConnection(user='streamer', password='streamer', db='streamer', conv=conversions, use_unicode = True, charset = "utf8", debug='false')
    sqlhub.processConnection = connection
# 	sqlhub.threadConnection = connection
     

if "Channel" not in globals():
    class Channel(SQLObject):
        vdrId = StringCol()
        name = StringCol()
        def to_json(self):
#             return json.dumps({u'id': str(self.id), u'name': str(self.name)}, sort_keys=True)
            return {u'id': str(self.id), u'name': str(self.name)}
 
if "FileSegment" not in globals():
	class FileSegment(SQLObject):
		fileId = BigIntCol()
		startTime = BigIntCol()
		duration = IntCol()
		channel = ForeignKey('Channel')
		firstLastIndex = DatabaseIndex('fileId', 'channel')
#		def __init__(self, fid, st, dur, ch):
#			self.fileId = fid
#			self.startTime = st
#			self.duration = dur
#			self.channel = ch
#		@classmethod
#		def from_json(cls, json_str):
#			json_dict = json.loads(json_str)
#			return cls(**json_dict)

if "Event" not in globals():       
    class Event(SQLObject):
        vdrId = IntCol()
        startTime = BigIntCol()
        endTime = BigIntCol()
        duration = IntCol()
        title = StringCol()
        shortText = StringCol()
        description = StringCol()
        channel = ForeignKey('Channel')
        def to_json(self):
#               return json.dumps({u'id': str(self.id), u'startTime': int(self.startTime), u'duration': int(self.duration)} 
#                                 , sort_keys=True)

            return {u'id': str(self.id), 
                               u'startTime': int(self.startTime),
                               u'startTime2': str(time.gmtime(self.startTime/1000)), 
                               u'duration': int(self.duration), 
                               u'title': self.title,
                                u'description': (self.shortText).decode('utf-8')
                               }
        


# app method
def wsgi_app(environ, start_response):
    """ Display the contents of the environ dictionary."""

    status = '200 OK'
#    print "ahoj"
#     parse input parameters
#     query = urlparse.parse_qs(environ['QUERY_STRING'])
#     ret = str(environ)
    ret = 'fuck'
#     response_headers = [('Content-Type', 'text/plain'), ('Access-Control-Allow-Origin','*')
#                         ('Content-Length', str(len(ret)))]


    response_headers = [('Content-Type', 'text/plain'),
                            ('Access-Control-Allow-Origin','*'),
                            ('Content-Length', str(len(ret)))
                            ]
#
    if environ['PATH_INFO'] == '/test':
#         ret = json.dumps({u'a': int(23423423)})
        ret = str(environ)
        response_headers = [('Content-Type', 'text/plain'), 
                            ('Access-Control-Allow-Origin','*'),
                            ('Content-Length', str(len(ret)))
                            ]
     
    if environ['SCRIPT_NAME'] == '/playlist.m3u8':
        ret = playlist(environ)
        response_headers = [('Content-Type', 'application/x-mpegURL'), 
                        ('Access-Control-Allow-Origin','*'),
                        ('Content-Length', str(len(ret))),
			('Cache-Control', 'max-age=0, no-cache, no-store, must-revalidate'),
			('Pragma', 'no-cache'),
			('Expires', 'Wed, 11 Jan 1984 05:00:00 GMT'),
                        ('Content-Length', str(len(ret)))
                        ]
#        response_headers = [('Content-Type', 'text/plain'), ('Access-Control-Allow-Origin','*'),
#                        ('Content-Length', str(len(ret)))]
 
    if environ['PATH_INFO'] == '/listChannels':
            ret = listChannels(environ)
            response_headers = [('Content-Type', 'application/json'),
                                ('Access-Control-Allow-Origin','*'),
			('Cache-Control', 'max-age=0, no-cache, no-store, must-revalidate'),
			('Pragma', 'no-cache'),
			('Expires', 'Wed, 11 Jan 1984 05:00:00 GMT'),
                            ('Content-Length', str(len(ret)))]


    if environ['PATH_INFO'] == '/getNextEvent':
        ret = getNextEvent(environ)
        response_headers = [('Content-Type', 'application/json'),
                            ('Access-Control-Allow-Origin','*'),
			('Cache-Control', 'max-age=0, no-cache, no-store, must-revalidate'),
			('Pragma', 'no-cache'),
			('Expires', 'Wed, 11 Jan 1984 05:00:00 GMT'),
                            ('Content-Length', str(len(ret)))]
      
    if environ['PATH_INFO'] == '/listChannel':
        ret = listChannel(environ)
        response_headers = [('Content-Type', 'application/json'),
                            ('Access-Control-Allow-Origin','*'),
							('Cache-Control', 'max-age=0, no-cache, no-store, must-revalidate'),
							('Pragma', 'no-cache'),
							('Expires', 'Wed, 11 Jan 1984 05:00:00 GMT'),
                            ('Content-Length', str(len(ret)))]
	
    if environ['PATH_INFO'] == '/addSegment':
        ret = addSegment(environ)
        
        response_headers = [('Content-Type', 'application/json'),
                            ('Access-Control-Allow-Origin','*'),
							('Cache-Control', 'max-age=0, no-cache, no-store, must-revalidate'),
							('Pragma', 'no-cache'),
							('Expires', 'Wed, 11 Jan 1984 05:00:00 GMT'),
                            ('Content-Length', str(len(ret)))]

    if environ['PATH_INFO'] == '/getLastSegment':
        ret = getLastSegment(environ)
        
        response_headers = [('Content-Type', 'application/json'),
                            ('Access-Control-Allow-Origin','*'),
							('Cache-Control', 'max-age=0, no-cache, no-store, must-revalidate'),
							('Pragma', 'no-cache'),
							('Expires', 'Wed, 11 Jan 1984 05:00:00 GMT'),
                            ('Content-Length', str(len(ret)))]
    
    #print environ
    start_response(status, response_headers)
    yield ret

def getLastSegment(environ):
	startFileIndex = 0
	query = urlparse.parse_qs(environ['QUERY_STRING'])
	#print query

	channelVdrId = ''
	channelVdrId = query['vdrId'][0]

	currentChannel = Channel.select(Channel.q.vdrId == channelVdrId)
	currentChannel = (list(currentChannel))[0]
	#print currentChannel

	#get last file index from db
	res = FileSegment.select("file_segment.id = (SELECT MAX( file_segment.id ) FROM file_segment WHERE channel_id=%s )" % currentChannel.id)


	result = list(res)

	if (len(result) > 0):
		print('last file in table:', result[0].fileId)
		startFileIndex = result[0].fileId + 1

	return json.dumps((startFileIndex, currentChannel.id))

def addSegment(environ):
	query = urlparse.parse_qs(environ['QUERY_STRING'])
	print query
	fileId = 0
	fileId = long(query['fileId'][0])
	startTime = 0
	startTime = long(query['startTime'][0])
	duration = 0
	duration = int(query['duration'][0])
	channelId = 0
	channelId = int(query['channel'][0])
	print FileSegment(fileId=fileId, startTime=startTime, channel=channelId, duration=duration)

	return json.dumps(True)

def getNextEvent(environ):
    query = urlparse.parse_qs(environ['QUERY_STRING'])
    eventId = 0
    if 'eventId' in query:
		eventId = int(query['eventId'][0])
    else:
		return json.dumps( 0, sort_keys=True, indent=4, separators=(',', ': '))

    selectedEvent = Event.get(eventId)

    res = Event.select(AND(Event.q.channel==selectedEvent.channel, Event.q.id >= selectedEvent.id), orderBy=Event.q.id).limit(2)

    nextEventId = res[1].id
    return json.dumps( nextEventId, sort_keys=True, indent=4, separators=(',', ': '))

def listChannel(environ):
    query = urlparse.parse_qs(environ['QUERY_STRING'])
    selectedChannelId = 1
    startTime = 0
    endTime = 0
    
    if 'channelId' in query:
        selectedChannelId = int(query['channelId'][0])
	selectedChannelId = convertToRightChannel(selectedChannelId)
    if ('startTime' in query) and ('endTime' in query):
        # specific data from db
        startTime = int(query['startTime'][0])
        endTime = int(query['endTime'][0])
      
#     max get time is 1 day
    maxDiff  = startTime + 24*60*60*1000
#     maxDiff  = startTime + 4*60*60*1000
    endTime = min(endTime, maxDiff)
        
        
    selectedChannel = Channel.get(selectedChannelId)
        
    res = Event.select(AND(Event.q.channel==selectedChannel, 
                                     Event.q.endTime>=startTime, 
                                     Event.q.endTime<=endTime), orderBy=Event.q.endTime)
    
    return json.dumps(list(res), sort_keys=True, indent=4, separators=(',', ': '))
            

def listChannels(environ):
    
    res = Channel.select()
    return json.dumps(list(res), sort_keys=True, indent=4, separators=(',', ': '))
# #     return str(list(res))
#     
# #     if 'channelId' in query:
# #         selectedChannelId = int(query['channelId'][0])
# #         
# #     if ('startTime' in query) and ('endTime' in query):
# #         # specific data from db
# #         startTime = int(query['startTime'][0])
# #         endTime = int(query['endTime'][0])
# #         
# #         
# #         
# #         return startTimeAndendTime(startTime, endTime, selectedChannelId)
#         
#     return ''

#  playlist.m3u8?eventId=123&quality=1,2,3
#  playlist.m3u8?eventId=123&quality=1
#  playlist.m3u8?eventId=123&quality=1


def playlist(environ):
    query = urlparse.parse_qs(environ['QUERY_STRING'])

    selectedQuality = 3
    # current time in ms
    currentTime = int(round(time.time() * 1000))


    if 'quality' in query:
        qualities = query['quality'][0].split(',')
        if len(qualities) == 1:
            selectedQuality = int(qualities[0])
        else:
            del query['quality']
            originalBasePath = (environ['SCRIPT_NAME'])[1:] + '?' + urllib.urlencode(query, True)

	    forceLive = 1
	    if ('eventId' in query):
		eventId = int(query['eventId'][0])
		forceLive = IsEventLive(eventId, currentTime)
            qualityList = []
            for q in qualities:
                qualityList.append(int(q))
            return generateRootM3U8(originalBasePath, qualityList, forceLive)

    selectedChannelId = 1
    
    if 'channelId' in query:
        selectedChannelId = int(query['channelId'][0])
	#selectedChannelId = convertToRightChannel(selectedChannelId)
          
    #print time.gmtime(currentTime/1000)

      
    # vod 
    if ('eventId' in query):
        # select speficif data from db
        # not supported
#         print('by eventId')
	continueEvent = 0
	try:
	    continueEvent = int(query['continueEvent'][0])
	except:
	    continueEvent = 0

	forceLive = 1
	try:
	    forceLive = int(query['live'][0])
	except:
	    forceLive = 1
        eventId = int(query['eventId'][0])
        return eventIdOnly(eventId, currentTime, selectedQuality, forceLive, continueEvent)
         
    elif ('startTime' in query) and ('endTime' in query):
        # specific data from db
        startTime = int(query['startTime'][0])
        endTime = int(query['endTime'][0])
        return startTimeAndendTime(startTime, endTime, selectedChannelId, selectedQuality)
 
    elif ('startTime' in query):
        # live stream from specific time => get only coupe files
        startTime = int(query['startTime'][0])
        endTime = startTime + 1000*60
        return startTimeOnly(startTime, endTime, selectedChannelId, selectedQuality)
 
    else: 
        # pure live stream
        # last 60s
	#res = live()
	selectedChannel = Channel.get(selectedChannelId)
        #return startTimeOnly(currentTime - 1000*60, currentTime, selectedChannelId)
	return live(selectedChannel, selectedQuality)
          
def live(selectedChannel, selectedQuality):
    res = FileSegment.select(FileSegment.q.channel==selectedChannel, orderBy=-FileSegment.q.startTime).limit(liveStreamParts)
    selectedChannelId = convertToRightChannelBack(selectedChannel.id)
#    print(res[-5:]);
#    res = Event.select(AND(Event.q.channel==selectedChannel, 
#                                     Event.q.endTime>=startTime, 
#                                     Event.q.endTime<=endTime), orderBy=Event.q.endTime)
    inverted = list(res)
    return generateM3U8list( inverted[::-1], selectedChannelId, False, True, selectedQuality )

def IsEventLive(eventId, currentTime):
    event = Event.get(eventId)
    selectedChannel = event.channel
    selectedChannelId = convertToRightChannelBack(selectedChannel.id)
    selectedChannel = Channel.get(selectedChannelId)    
    
    durationMs = (event.duration+defaultEventSuffixTime) * 1000

    res = FileSegment.select(AND(FileSegment.q.channel==selectedChannel, 
                                     FileSegment.q.startTime>=event.startTime, 
                                     FileSegment.q.startTime<=(event.startTime + durationMs)))
    result = list(res)
    if (len(result)>0):
        if (event.startTime + durationMs) > currentTime:
	    return True
	else:
	    return False
    return True

def eventIdOnly(eventId, currentTime, selectedQuality, forceLive, continueEvent):
    event = Event.get(eventId)
    selectedChannel = event.channel
    selectedChannelId = convertToRightChannelBack(selectedChannel.id)
    selectedChannel = Channel.get(selectedChannelId)    

    startTime = 0
    endTime = 0

    if(continueEvent == 1):
	startTime = event.startTime + (defaultEventSuffixTime * 1000)
	durationMs = (event.duration+defaultEventSuffixTime) * 1000
	endTime = event.startTime + durationMs
    else:
	startTime = event.startTime
	durationMs = (event.duration+defaultEventSuffixTime) * 1000
	endTime = event.startTime + durationMs

    res = FileSegment.select(AND(FileSegment.q.channel==selectedChannel, 
                                     FileSegment.q.startTime>=startTime, 
                                     FileSegment.q.startTime<=endTime))
    result = list(res)

    if (len(result)>0):
        #  check if event still running
        if (event.startTime + durationMs) > currentTime:
	    return generateM3U8list( result, selectedChannel.id, False, True, selectedQuality )

	if (forceLive == 1):
	    return generateM3U8list( result, selectedChannel.id, True, True, selectedQuality )

        return generateM3U8list( result, selectedChannel.id, True, False, selectedQuality )
	

    print "eventIdOnly go live as result is empty"
    # if no result => live stream
    #currentTime = int(round(time.time() * 1000))
    #return startTimeOnly(currentTime - 1000*60, currentTime, selectedChannel.id)
    return live(selectedChannel, selectedQuality)

def startTimeOnly( startTime, endTime, channelId, selectedQuality ):
    selectedChannel = Channel.get(channelId)

#    selectedChannelId = convertToRightChannelBack(selectedChannel.id)
#    selectedChannel = Channel.get(selectedChannelId)    


    res = FileSegment.select(AND(FileSegment.q.channel==selectedChannel, 
                                     FileSegment.q.startTime>=startTime,
                                     FileSegment.q.startTime<=endTime))
    result = list(res)
    return generateM3U8list( result, selectedChannel.id, False, True, selectedQuality )
    

def startTimeAndendTime( startTime, endTime, channelId, selectedQuality):
    selectedChannel = Channel.get(convertToRightChannelBack(channelId))

    res = FileSegment.select(AND(FileSegment.q.channel==selectedChannel, 
                                     FileSegment.q.startTime>=startTime, 
                                     FileSegment.q.startTime<=endTime))
    result = list(res)
    return generateM3U8list( result, selectedChannel.id, True, False, selectedQuality )

def formatQuality(quality):
    if quality == 1:
        return '005'
    if quality == 2:
        return '030'
    if quality == 3:
        return '100'
    return '100'

def formatFileName(fileIndex):
    return '%010d.ts' % fileIndex

def generateM3U8list( fileList, channelId, vod, live, selectedQuality ):
    if(len(fileList)==0):
	       return ' nic rozumneho !'

    dir = '/' + formatQuality(selectedQuality) + '/'

    res = ''
    res += '#EXTM3U\n'
    res += '#EXT-X-VERSION:7\n'
    res += '#EXT-X-ALLOW-CACHE:YES\n'
    res += '#EXT-X-TARGETDURATION:' + str(defaultStreamDuration) + '\n'

    
    if (vod == False) and (live == False):
#	res += '#EXT-X-MEDIA-SEQUENCE:0\n'
        res += '#EXT-X-MEDIA-SEQUENCE:' + str(fileList[0].fileId) + '\n'

    if( live == True):
	res += '#EXT-X-PLAYLIST-TYPE:EVENT\n'
	#offset = -10.0 * (len(fileList))
	offset = -1.0
	res += '#EXT-X-START:TIME-OFFSET=' + str(offset)+'\n'
    else:
	res += '#EXT-X-PLAYLIST-TYPE:VOD\n'

#    res += '#EXT-X-MEDIA-SEQUENCE:0\n'
#    res += '#EXT-X-MEDIA-SEQUENCE:' + str(fileList[0].fileId) + '\n'

    
    for i in fileList:
        res +=  '#EXTINF:' + str(defaultStreamDuration) + ',\n'
        res +=  str(channelId) + dir + formatFileName(i.fileId) + '\n'

#        res +=  '../' + str(channelId) + dir + formatFileName(i.fileId) + '\n'

#    res +=  '#EXTINF:' + str(10) + ',\n'
#    res +=  '../' + '5' + '/' + '0000000000.ts' + '\n'

    if vod == True:
        res +=  '#EXT-X-ENDLIST\n'

    return res

def generateRootM3U8(originalbasepath, qualityList, forceLive):
    if(len(qualityList)==0):
	       return ' nic rozumneho !'
    res = ''
    res += '#EXTM3U\n'
    res += '#EXT-X-VERSION:7\n'

    supportedBitrate = True
    for q in qualityList:
        supportedBitrate = True
        # res += '#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=232370,CODECS=\"mp4a.40.2, avc1.4d4015\"\n'
        # res += '#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=10000\n'
        #if q == 1:
        #    res += '#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=10000,RESOLUTION=180x160\n'
        #el
	if q == 2:
            res += '#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=460000,RESOLUTION=360x288\n'
        elif q == 3:
            res += '#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=990000,RESOLUTION=720x576\n'
	else :
	    supportedBitrate = False

	if supportedBitrate == True:
	    if forceLive:
		res += originalbasepath + '&quality=' + str(q) + '&live=1\n'
	    else:
		res += originalbasepath + '&quality=' + str(q) + '&live=0\n'

    return res

def convertToRightChannel(channelId):
    newChannelId = channelId
    if(newChannelId == 49):
        newChannelId = 36
    if(newChannelId == 50):
        newChannelId = 40
    return newChannelId

def convertToRightChannelBack(channelId):
    newChannelId = channelId
    if(newChannelId == 36):
        newChannelId = 49
    if(newChannelId == 40):
        newChannelId = 50
    return newChannelId


# mod_wsgi need the *application* variable to serve our small app
application = wsgi_app
    
